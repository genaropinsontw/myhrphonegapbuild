'use strict';

/* App Module */

//var app = angular.module('MyHR', [ 'ajoslin.mobile-navigate','ngMobile' ] ).

var app = angular.module('MyHR', [ 'ngMobile' ] ).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.
      when('/home', {templateUrl: 'view/home.html',   controller: MessageListController}).
      when('/home/message/:id', {templateUrl: 'view/message-detail.html', controller: MessageDetailController}).
      when('/help', {templateUrl: 'view/help.html', controller: HelpListController}).
      when('/help/question/:topicLabel/:questionID', {templateUrl: 'view/help-detail.html', controller: HelpDetailController}).
      otherwise({redirectTo: '/home'});
}]);



app.factory('sharedProperties' , [ '$http' , function( $http ) {
    
    var MESSAGES_URL = "js/model/messages.json";
    var QUESTIONS_URL = "js/model/questions.json";
    var selectedMessage = {};
    var messages = new Array();
    var selectedQuestion = {};
    var previousSelectedQuestion = {};
    var questions = new Array();
    var isHelpJSONRetrieved = false;

    return {
        setMessages : function( newValue ) {
            messages = newValue;
        },
        getMessages : function() 
        {
            if ( messages.length === 0 )
            {
                return $http.get(MESSAGES_URL).then(function(response) {
                var data = response.data;
                for ( var i=0; i < data.length ; i++ )
                {
                    data[i].date = new Date(data[i].date);
                }
                    messages = data;
                    return messages;
               });
            }
            else
            {
                return messages;
            }
        },
        setSelectedMessage : function( newValue ) 
        {
            selectedMessage = newValue;
        },
        getSelectedMessage : function() 
        {
            return selectedMessage;
        }, 
        setQuestions : function( newValue ) 
        {
            questions = newValue;
        },
        getQuestions : function() 
        {
            if ( questions.length === 0 )
            {
                return $http.get(QUESTIONS_URL).then( function(response) {
                var data = response.data;
                questions = data;
                isHelpJSONRetrieved = true;
                return questions;
               });
            }
            else
            {
                return questions;
            }
        },
        setSelectedQuestion : function( newValue ) 
        {
            previousSelectedQuestion = selectedQuestion;
            selectedQuestion = newValue;
        },
        getSelectedQuestion : function() 
        {
            return selectedQuestion;
        },
        getIsHelpJSONRetrieved : function() 
        {
            return isHelpJSONRetrieved;
        }
    };

}]);

//The shared global methods on this block of code
//can be invoked by the different controllers through 
//the controller's $scope ONLY.
app.run( function($rootScope,$location,sharedProperties) 
{
    $rootScope.openInformation = function() 
    {
        $("#information").modal('show'); 
    };

    $rootScope.openMessageDeleteConfirmation = function()
    {
        $("#deleteConfirmation").modal('show'); 
    };
    
    $rootScope.doTransition = function(dir,path) 
    {
        if ( dir != '' && path != '' )
            {
                $rootScope.animationDirection = dir;
                $location.path(path);                
            }
    };
    
    $rootScope.toggleSelectedMessageIsActive = function( message , isActive )
    {
        var selectedMessage;
        if ( message != null )
        {
            sharedProperties.setSelectedMessage( message );
            selectedMessage = sharedProperties.getSelectedMessage();
        }
        else
        {
            selectedMessage = sharedProperties.getSelectedMessage();
        }
        
        selectedMessage.isActive = isActive;
    };
    
    $rootScope.deleteSelectedMessage = function()
    {
        var selectedMessage = sharedProperties.getSelectedMessage();
        
        if ( selectedMessage != null )
        {
            if ( selectedMessage.hasOwnProperty('id') )
            {
                var messages = sharedProperties.getMessages();
                
               for ( var i=0; i < messages.length ; i++ )
                {
                    if ( messages[i]['id'] == selectedMessage.id )
                    {
                           messages.splice(i, 1);
                           break;
                    }
                }
                
                sharedProperties.setSelectedMessage( null );
                
            }
        }
    };
    
    $rootScope.isHomeEditable = false;
        
});

