'use strict';

function MessageListController($scope, $http , $location , $rootScope , sharedProperties ) 
{
    $scope.messages = sharedProperties.getMessages();
    $scope.homeTransition = '';
    $scope.helpTransition = 'forward';
    $scope.showBackButton = false;
    $scope.backURL = "";
    
   $scope.updateSelectedMessage = function ( value )
   {
        value.isRead = true;
        sharedProperties.setSelectedMessage( value );
        $scope.doTransition( 'forward', '/home/message/'+value.id );
   };
   
   $scope.toggleEditableState = function ( isEditable )
   {
       $rootScope.isHomeEditable = isEditable;
   };
   
    $scope.deleteMessage = function()
    {
      $rootScope.deleteSelectedMessage();  
    };
   
}
   
function MessageDetailController( $scope, $location, $rootScope , $routeParams , sharedProperties ) 
{
  $scope.message = sharedProperties.getSelectedMessage();
  $scope.homeTransition = 'backward';
  $scope.helpTransition = 'forward';
  $scope.showBackButton = true;
  $scope.backURL = "/home";
  
  $scope.deleteMessage = function()
  {
    $rootScope.deleteSelectedMessage();  
    $rootScope.doTransition( $scope.homeTransition , "/home" );  
  };
  
}

function HelpListController( $scope, $http , $rootScope , $location , sharedProperties ) 
{
    $scope.homeTransition = 'backward';
    $scope.helpTransition = 'forward';
    $scope.showBackButton = false;
    $scope.backURL = "";
      
    if ( sharedProperties.getIsHelpJSONRetrieved() )
    {
        $scope.questions = sharedProperties.getQuestions();
        for ( var i=0; i < $scope.questions.length ; i++ )
        {
               $scope['topicLabel'+i] = $scope.questions[i].topicLabel;
               $scope['topicQuestions'+i] = $scope.questions[i].topicQuestions;
        }          
    }
    else
    {
        $scope.questions = sharedProperties.getQuestions().then( function ( questions ) 
        {  
            for ( var i=0; i < questions.length ; i++ )
            {
                   $scope['topicLabel'+i] = questions[i].topicLabel;
                   $scope['topicQuestions'+i] = questions[i].topicQuestions;
            }
            return questions;
        });                
    }
        
    $scope.updateSelectedQuestion = function ( selectedQuestion )
    {
         sharedProperties.setSelectedQuestion( selectedQuestion );
        $scope.doTransition( 'forward' , '/help/question/'+selectedQuestion.topicLabel+"/"+selectedQuestion.questionID);
    };
    

    
}

function HelpDetailController( $scope, $location , $rootScope , $routeParams , sharedProperties ) 
{   
    $scope.homeTransition = 'backward';
    $scope.helpTransition = 'backward';
    $scope.showBackButton = true;
    $scope.backURL = "/help";
    
    $scope.question = sharedProperties.getSelectedQuestion();
    
}

